def calculate_polish(polish_notation):
  # Преобразуем строку в список, перевернем и удалим пустые элементы
  reverse_polish_notation_list = [op for op in polish_notation.split(' ')[::-1] if op != '']
  # print(reverse_polish_notation_list)

  stack = []
  operators = list('+-*/')
  try:
    for op in reverse_polish_notation_list:
      if op in operators:
        if len(stack) < 2:
          raise ArithmeticError('Ошибка в выражении с польской нотацией')
        else:
          right = stack.pop()  # на верху стека правый операнд (важно для операций - и /)
          left = stack.pop()  # а следующий левый
    
          # для безопасности использования eval необходимо, чтобы операнд был приводим и приведен к числу
          stack.append(str(eval(left + op + right)))
      else:
        if op.isdigit():
          # op = str(float(op))
          op = str(int(op))
        else:
          raise TypeError('В качестве операнда должно быть число')

        assert float(op) >= 0, 'Использовано отрицательно число'
        stack.append(op)
      # print(op, stack)

    if len(stack) > 1:
      raise ArithmeticError('Ошибка в выражении с польской нотацией')
    else:
      # print('Результат вычисления:', stack[0])
      return stack[0]

  except Exception as e:
    print('{}: {}'.format(type(e).__name__, e))


# polish_notation = '/ - 4 + * 2 3 1 + * 5 2 8'  # корректное выражение (результат: 6)
# polish_notation = '+ -5 4'  # использование отрицательного числа
# polish_notation = '+ - 5 4'  # ошибка в порядке и/или количестве операндов
# polish_notation = '+ 5 4 7'  # ошибка в порядке и/или количестве операндов

polish_notation = input('Введите выражение в польской нотации')
print('Результат вычисления:', calculate_polish(polish_notation))
