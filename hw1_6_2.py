documents = [
  {"type": "passport", "number": "2207 876234", "name": "Василий Гупкин"},
  {"type": "invoice", "number": "11-2", "name": "Геннадий Покемонов"},
  {"type": "insurance", "number": "10006", "name": "Аристарх Павлов"}
]

directories = {
  '1': ['2207 876234', '11-2'],
  '2': ['10006'],
  '3': []
}

# raise SystemExit

# print ('Задача 1:\n')

def belong_document(documents_list, number_document=None):
  '''
  p – people – команда, которая спросит номер документа и выведет имя человека, которому он принадлежит.

  Т.к. не гарантируется уникальность номеров в рамках всех документов, возможно существование нескольких документов с одним номером принадлежащие разым людям, будем выводить их списком 
  '''

  if number_document == None:
    number_document = input("Введите номер документа для проверки")

  find_documents = [item for item in documents_list if item['number'] == number_document]

  if len(find_documents) == 0:
    print('Документа с таким номером не существует')
  else:
    print("Документ(ы) с номером {} принадлежит(ат):".format(number_document))
    for document in find_documents:
      print(' ', document['name'])

#print('1.1 Принадлежность документа')
#belong_document(documents, "11-2")


def show_all_documents(documents_list):
  '''
  l– list – команда, которая выведет список всех документов в формате passport "2207 876234" "Василий Гупкин".
  '''
  for document in documents_list:
    print('{} "{}" "{}"'.format(document['type'], document['number'], document['name']))

# print('\n1.2 Форматированный список документов')
# show_all_documents(documents)


def find_shelf(directories_dict, number_document=None):
  '''
  s – shelf – команда, которая спросит номер документа и выведет номер полки, на которой он находится.
  
  Предполагаю возможность существования нескольких разных документов с одинаковым номером находящиеся на разных полках.
  '''

  if number_document == None:
    number_document = input("Введите номер документа для поиска полки")

  shelves_list = [key for key, value in directories.items() if number_document in value]
  if len(shelves_list) == 0:
    print('Документ отсутствует на полках')
  else:
    print("Документ(ы) с номером {} расположен(ы) на полке(ах): {}".format(number_document, ', '.join(shelves_list)))

# print('\n1.3 Принадлежность документа')
# find_shelf(directories, "11-2")


def add_document(documents_list, directories_dict, new_document=None, number_shelf=None):
  '''
  a – add – команда, которая добавит новый документ в каталог и в перечень полок, спросив его номер, тип, имя владельца и номер полки, на котором он будет храниться
  '''

  if new_document == None: 
    number_document = input("Введите номер документа")
    type_document = input("Введите тип документа")
    name_owner = input("Введите имя владельца")

    new_document = {
      "type": type_document, 
      "number": number_document,
      "name": name_owner
    }

  if number_shelf == None:
    number_shelf = input("Введите номер полки")
  
  documents_list.append(new_document)
  print('Документ создан')

  if number_shelf not in directories_dict:
    print('Полки с номером {} не существует'.format(number_shelf))
    # return
    directories_dict[number_shelf] = []
    print('Она была добавлена')

  directories_dict[number_shelf].append(new_document['number'])
  print('Документ добавлен на полку')

# print('\n1.4 Добавление документа')
# new_document = {
#   'type': 'birth certificate',
#   'number': '2207 876234',
#   'name': 'Сидор Петрович'
# }
# add_document(documents, directories, new_document, '3')

# print('\nНовый список документов:\n', documents)
# print('\nНовое расположение на полках:\n', directories)


# print('\n\nЗадача 2:')

def delete_document(documents_list, directories_dict, number_document=None):
  '''
  d – delete – команда, которая спросит номер документа и удалит его из каталога и из перечня полок

  Документов с одинаковым номером может быть несколько в списке и на полке.
  Также необходимо модифицировать переданные по ссылки список и словарь
  Чтобы избежать дублирования кода удаление из списков выполняется отдельной функцией
  '''

  def delete_all_list(basic_list, condition_lambda):
    '''
    Вспомогательная процедура для удаления всех элементов списка по заданному условию.
    Для возможности удаления разных по структуре элементов
    '''
    indexes_delete = [index for index, item in enumerate(basic_list) if condition_lambda(item)]
    for index in sorted(indexes_delete, reverse=True):
      del basic_list[index]


  if number_document == None:
    number_document = input("Введите номер документа для удаления")

  delete_all_list(documents_list, lambda item: item['number'] == number_document)
  print('Документ удален из списка')

  condition = lambda item: item == number_document
  for current_list in directories_dict.values():
    delete_all_list(current_list, condition)

  print('Документ удален с полки')
 
# print('\n2.1 Удаление документа из списка и с полок')
# delete_document(documents, directories, '11-2')

# print('\nCписок документов после удаления документа:\n', documents)
# print('\nПолки после удаления документа:\n', directories)


def move_document(directories_dict, number_document=None,  number_shelf=None):
  '''
  m – move – команда, которая спросит номер документа и целевую полку и переместит его с текущей полки на целевую
  
  Предполагаю возможность существования нескольких разных документов с одинаковым номером находящиеся на разных полках.
  '''

  if number_document == None:
    number_document = input("Введите номер документа для перемещения")

  if number_shelf == None:
    number_shelf = input("Введите наименование полки куда положить документ")

  if number_shelf not in directories_dict:
    print('Полки с номером {} не существует'.format(number_shelf))
    # return
    directories_dict[number_shelf] = []
    print('Она была добавлена')
  
  for current_shelf, documents_list in directories_dict.items():
    if current_shelf == number_shelf:
      continue
    
    while number_document in documents_list:
      del documents_list[documents_list.index(number_document)]
      
      directories_dict[number_shelf].append(number_document)
  

# print('\n2.2 Перемещение документа')
# move_document(directories, '2207 876234', '2')

# print('Полки после перемещени документа:\n', directories)


def add_shelf(directories_dict, new_number_shelf=None):
  '''
  as – add shelf – команда, которая спросит номер новой полки и добавит ее в перечень
  '''

  if new_number_shelf == None:
    new_number_shelf = input("Введите номер новой полки")

  if new_number_shelf in directories_dict:
    print('Полка с таким номером уже существует')
    return

  directories_dict[new_number_shelf] = []

# print('\n2.3 Добавление новой полки')
# add_shelf(directories, '7')

# print('Полки после добавления новой:\n', directories)

def show_owner_documents(documents_list):
  '''
  Функция - реализация задачи №3 к лекции "Исключения"
  po - people owner - команда выведет всех владельцев документов, с проверкой существования в документе поля 'name'
  '''
  try:
    owner_set = set([item['name'] for item in documents_list])
  except:
    print('Обнаружен документ не имеющий поля владельца "name"')
    return

  print('Владельцы документов:')
  for owner in sorted(owner_set):
    print(' ', owner)
  # owner_set = 

# Для демонстрации обработки исключения в список документов необходимо добавить ошибочные данные
documents.append({"type": "passport", "number": "2741 352085", "Name": "Василий Гупкин"})
# documents.append({"type": "passport", "number": "4982 134921", "name": "Аристарх Павлов"})
# show_owner_documents(documents)

commands = {
  'e': {'name': 'exit'},
  'p': {'name': 'people', 'func': belong_document, 'args': [documents]},
  's': {'name': 'shelf', 'func': find_shelf, 'args': [directories]},
  'a': {'name': 'add', 'func': add_document, 'args': [documents, directories]},
  'd': {'name': 'delete', 'func': delete_document, 'args': [documents, directories]},
  'm': {'name': 'move', 'func': move_document, 'args': [directories]},
  'as': {'name': 'add shelf', 'func': add_shelf, 'args': [directories]},
  'po': {'name': 'people owner', 'func': show_owner_documents, 'args': [documents]}
}

def show_commands(commands):
  for key, command in commands.items():
    print('{} - {}   \t'.format(key, command['name']), end='')
  print()

def show_data(data):
  for name, item in data.items():
    print('{}:\n{}\n'.format(name, item))

data = {
  'documents': documents,
  'directories': directories
}

while True:
  show_data(data)
  show_commands(commands)

  command = None
  while True:
    command = input('Введите команду:').lower()
    if command in commands:
      break
    else:
      print('Данная команда не известна')

  if command == 'e':
    break
  
  print()
  desc_command = commands[command]
  desc_command['func'](*desc_command['args'])

  print('\n')
